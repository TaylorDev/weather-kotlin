package com.incodeit.root.weatherkotlin.ui.base

import android.content.Context
import android.os.Bundle
import butterknife.Unbinder
import com.incodeit.root.weatherkotlin.di.component.ActivityComponent
import timber.log.Timber

abstract class BaseFragment : android.support.v4.app.Fragment() , IBaseView {
    
    lateinit var unBinder : Unbinder
    lateinit var activity : BaseActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity) {
            this.activity = context
            context.onFragmentAttached()
        }
    }

    override fun onDestroy() {
        unBinder.unbind()
        super.onDestroy()
    }

    fun getActivityComponent() : ActivityComponent? {
        return activity.getActivityComponents()
    }

    override fun onError(errorString: String) {
        Timber.d("onError %s", errorString)
    }

    override fun onError(resId: Int) {
        Timber.d("onError %s", getString(resId))
    }

    override fun networkConnected(): Boolean {
        return true
    }
}