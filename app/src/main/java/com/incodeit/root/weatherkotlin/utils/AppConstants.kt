package com.incodeit.root.weatherkotlin.utils

object AppConstants {

    const val API_STATUS_CODE_LOCAL_ERROR = 0
    const val BASE_URL = "http://api.openweathermap.org/"
    const val QUERY_FORECAST = "/data/2.5/forecast"
    const val QUERY_FORECAST_DAILY = "/data/2.5/forecast/daily"
    const val QUERY_WEATHER = "/data/2.5/weather"
    const val CITY_NAME = "q"
    const val UNITS_QUERY = "units"
    const val LANG_QUERY = "lang"
    const val APP_ID_QUERY = "appid"
    const val UNITS = "metric"
    const val LANG = "ru"
    const val APP_ID = "d47df1d95eca0658a3d9442b8f12ec62"

    const val CELSIUS = "c"
    const val FAHRENHEIT = "f"
    const val PERCENT = " %"
    const val PRESSURE = " hPa"
    const val SPEED = "m/s"
    const val SPEED_US = "Mi/hour"
    const val CELSIUS_DATA = "C"
    const val FAHRENHEIT_DATA = "F"
    const val SETTING_USER = "setting_user"
    const val DEFAULT_CITY = "Kiev"

    const val DAY = "day"
    const val THREE_DAY = "threeDay"
    const val WEEK = "week"

    const val TEMPERATURE_PREF = "temperature"
    const val FREQUENCY_PREF= "frequency"
    const val SPEED_PREF = "speed"
    const val CITY_PREF = "city"
    const val REGION_PREF = "region"

}