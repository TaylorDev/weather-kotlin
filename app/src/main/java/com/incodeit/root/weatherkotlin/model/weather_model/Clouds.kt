package com.incodeit.root.weatherkotlin.model.weather_model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Clouds {

    @SerializedName("all")
    @Expose
    private val all : Double? = null

    fun getAll() : Double? {
        return this.all
    }

}