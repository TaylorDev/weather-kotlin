package com.incodeit.root.weatherkotlin.di.component

import com.incodeit.root.weatherkotlin.ui.weather.WeatherActivity
import com.incodeit.root.weatherkotlin.di.module.ActivityModule
import com.incodeit.root.weatherkotlin.di.ActivityScope
import com.incodeit.root.weatherkotlin.ui.day_fragment.DayWeatherFragment
import com.incodeit.root.weatherkotlin.ui.hour_weather_fragment.HourWeatherFragment
import com.incodeit.root.weatherkotlin.ui.setting.SettingActivity
import com.incodeit.root.weatherkotlin.ui.splash.SplashActivity
import com.incodeit.root.weatherkotlin.ui.three_days.ThreeDaysFragment
import com.incodeit.root.weatherkotlin.ui.week_fragment.WeekFragment
import dagger.Component

@ActivityScope
@Component(dependencies = arrayOf(ApplicationComponent::class) , modules = arrayOf(ActivityModule::class))
interface ActivityComponent  {

    fun inject(activity: WeatherActivity)

    fun inject(activity: SplashActivity)

    fun inject(fragment: DayWeatherFragment)

    fun inject(activity: SettingActivity)

    fun inject(fragment: ThreeDaysFragment)

    fun inject(fragment: WeekFragment)

    fun inject(fragment: HourWeatherFragment)

}