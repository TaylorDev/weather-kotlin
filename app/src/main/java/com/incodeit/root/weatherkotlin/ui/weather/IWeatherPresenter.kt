package com.incodeit.root.weatherkotlin.ui.weather

import android.app.Activity
import com.incodeit.root.weatherkotlin.di.ActivityScope
import com.incodeit.root.weatherkotlin.ui.base.IBasePresenter

@ActivityScope
interface IWeatherPresenter<in V : IWeatherView> : IBasePresenter<V> {

    fun getUserPreference(activity: Activity): ArrayList<String>

}