package com.incodeit.root.weatherkotlin.ui.week_fragment

import com.incodeit.root.weatherkotlin.adapter.Weather.DailyWeatherRecyclerAdapter
import com.incodeit.root.weatherkotlin.data.IDataManager
import com.incodeit.root.weatherkotlin.model.daily_model.DailyModelUi
import com.incodeit.root.weatherkotlin.ui.base.BasePresenter
import com.incodeit.root.weatherkotlin.utils.rx.ISchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class WeekPresenter<V : IWeekView> @Inject
constructor(schedulerProvider: ISchedulerProvider,
            compositeDisposable: CompositeDisposable,
            dataManager: IDataManager)
    : BasePresenter<V>(schedulerProvider , compositeDisposable , dataManager) , IWeekPresenter<V> {

    private lateinit var adapter: DailyWeatherRecyclerAdapter

    override fun initAdapter(): DailyWeatherRecyclerAdapter {
        adapter = DailyWeatherRecyclerAdapter()
        adapter.addAll(DailyModelUi.Companion.listDaily)
        return adapter
    }

}