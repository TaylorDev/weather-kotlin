package com.incodeit.root.weatherkotlin.ui.setting

import com.incodeit.root.weatherkotlin.ui.base.IBaseView

interface ISettingView : IBaseView {

    fun initComponent()

    fun userPref()

    fun initSearch()

}