package com.incodeit.root.weatherkotlin.adapter.Weather

import android.view.ViewGroup

import com.incodeit.root.weatherkotlin.model.daily_model.Daily
import com.incodeit.root.weatherkotlin.model.daily_model.WeatherDailyParameter

import zlc.season.practicalrecyclerview.AbstractAdapter
import zlc.season.practicalrecyclerview.AbstractViewHolder
import zlc.season.practicalrecyclerview.ItemType

class DailyWeatherRecyclerAdapter : AbstractAdapter<ItemType , AbstractViewHolder<*>>() {

    override fun onNewCreateViewHolder(parent: ViewGroup , viewType: Int): AbstractViewHolder<*>? {
        if (viewType == RecyclerItemType.PARENT.value) {
            return ParentViewHolder(this , parent)
        } else if (viewType == RecyclerItemType.CHILD.value) {
            return ChildViewHolder(parent)
        }
        return null
    }

    override fun onNewBindViewHolder(holder: AbstractViewHolder<*> , position: Int) {
        (holder as? ParentViewHolder)?.setData(get(position) as Daily) ?: (holder as? ChildViewHolder)?.setData(get(position) as WeatherDailyParameter)
    }

}