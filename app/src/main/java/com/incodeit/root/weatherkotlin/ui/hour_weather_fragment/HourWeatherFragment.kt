package com.incodeit.root.weatherkotlin.ui.hour_weather_fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.incodeit.root.weatherkotlin.R
import com.incodeit.root.weatherkotlin.adapter.HourWeather.HourWeatherRecyclerAdapter
import com.incodeit.root.weatherkotlin.adapter.Weather.ParentViewHolder
import com.incodeit.root.weatherkotlin.adapter.Weather.ParentViewHolder.Companion.position
import com.incodeit.root.weatherkotlin.data.ConverterParameters
import com.incodeit.root.weatherkotlin.model.daily_model.Daily
import com.incodeit.root.weatherkotlin.model.daily_model.DailyModelUi
import com.incodeit.root.weatherkotlin.model.daily_model.WeatherDailyParameter
import com.incodeit.root.weatherkotlin.ui.base.BaseFragment
import com.incodeit.root.weatherkotlin.ui.weather.WeatherActivity
import com.incodeit.root.weatherkotlin.utils.AppConstants
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import javax.annotation.Nullable
import javax.inject.Inject

class HourWeatherFragment : BaseFragment(), IHourWeatherView {

    @BindView(R.id.recyclerViewHourWeather)
    @Nullable @JvmField var recyclerViewHour: RecyclerView? = null

    @BindView(R.id.pressure_data_daily)
    @Nullable @JvmField var pressureDataDaily: TextView? = null

    @BindView(R.id.humidity_data_daily)
    @Nullable @JvmField var humidityDataDaily: TextView? = null

    @BindView(R.id.wind_data_daily)
    @Nullable @JvmField var windDataDaily: TextView? = null

    @BindView(R.id.cloudiness_data_daily)
    @Nullable @JvmField var cloudinessDataDaily: TextView? = null

    @BindView(R.id.dateDay)
    @Nullable @JvmField var dateDayTextView: TextView? = null

    @Inject
    lateinit var presenter: HourWeatherPresenter<IHourWeatherView>

    companion object {

        var TAG = "HourWeatherFragment"

        fun newInstance(positionItem : Int) : HourWeatherFragment{
            val args = Bundle()
            position = positionItem
            val fragment = HourWeatherFragment()
            fragment.arguments = args
            return fragment
        }

    }

    @SuppressLint("SimpleDateFormat" , "SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater? , container: ViewGroup? ,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_hour_weather , container , false)
        val component = getActivityComponent()
        if (component != null) {
            component.inject(this)
            unBinder = ButterKnife.bind(this, view)
            presenter.onAttach(this)
        }
        val originalFormat = SimpleDateFormat("EEE, MMM d, ''yy")
        val targetFormat = SimpleDateFormat("EEE, d MMMM yyyy")

        try {
            dateDayTextView?.text = targetFormat.format(originalFormat.parse(presenter.getDaily().dateDaily))
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        recyclerViewHour?.layoutManager = LinearLayoutManager(view.context)
        recyclerViewHour?.adapter = presenter.getHourAdapter()

        pressureDataDaily?.text = presenter.weatherDailyParameter.pressureDaily + AppConstants.PRESSURE
        humidityDataDaily?.text = presenter.weatherDailyParameter.humidityDaily + AppConstants.PERCENT
        if(WeatherActivity.pref[2].equals(AppConstants.SPEED)) {
            windDataDaily?.text = presenter.weatherDailyParameter.windDaily + " " + getString(R.string.km)
        } else {
            windDataDaily?.text = DecimalFormat("#0.00").format(ConverterParameters.getMiles(presenter.weatherDailyParameter.windDaily!!)) + " " + getString(R.string.mi)
        }
        cloudinessDataDaily?.text = presenter.weatherDailyParameter.cloudinessDaily + AppConstants.PERCENT
        return view
    }

    override fun networkConnected(): Boolean {
        return false
    }

}