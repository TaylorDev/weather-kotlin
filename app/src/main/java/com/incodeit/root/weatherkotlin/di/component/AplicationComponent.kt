package com.incodeit.root.weatherkotlin.di.component

import android.content.Context
import com.incodeit.root.weatherkotlin.data.IDataManager
import com.incodeit.root.weatherkotlin.app.WeatherApplication
import com.incodeit.root.weatherkotlin.di.module.AppModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface ApplicationComponent {

    fun inject(app: WeatherApplication)

    fun context(): Context

    fun dataManager(): IDataManager

    fun application(): WeatherApplication
}