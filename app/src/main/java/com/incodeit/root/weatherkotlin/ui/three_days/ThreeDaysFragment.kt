package com.incodeit.root.weatherkotlin.ui.three_days

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.incodeit.root.weatherkotlin.R
import com.incodeit.root.weatherkotlin.model.daily_model.Daily
import com.incodeit.root.weatherkotlin.ui.base.BaseFragment
import android.support.v7.widget.LinearLayoutManager
import butterknife.BindView
import com.incodeit.root.weatherkotlin.adapter.Weather.DailyWeatherRecyclerAdapter
import zlc.season.practicalrecyclerview.PracticalRecyclerView
import com.incodeit.root.weatherkotlin.model.daily_model.DailyModelUi
import butterknife.ButterKnife
import javax.annotation.Nullable
import javax.inject.Inject

class ThreeDaysFragment : BaseFragment(), IThreeDaysView {

    @BindView(R.id.recyclerViewThreeDay)
    @Nullable @JvmField var recyclerView: PracticalRecyclerView? = null

    @Inject
    lateinit var presenter: ThreeDaysPresenter<IThreeDaysView>

    companion object {
        val TAG = "ThreeDaysFragment"

        fun newInstance(): ThreeDaysFragment {
            val args = Bundle()
            val fragment = ThreeDaysFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater? , container: ViewGroup? ,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_three_days , container , false)
        val component = getActivityComponent()
        if (component != null) {
            component.inject(this)
            unBinder = ButterKnife.bind(this , view)
            presenter.onAttach(this)
        }

        recyclerView?.setLayoutManager(LinearLayoutManager(view.context))
        recyclerView?.setAdapter(presenter.getAdapter())
        return view
    }

}