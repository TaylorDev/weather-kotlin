package com.incodeit.root.weatherkotlin.ui.weather

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.view.Menu
import android.view.View
import android.view.WindowManager
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.incodeit.root.weatherkotlin.R
import com.incodeit.root.weatherkotlin.adapter.Weather.ParentViewHolder
import com.incodeit.root.weatherkotlin.data.ConverterParameters
import com.incodeit.root.weatherkotlin.mapper.ConditionMapper
import com.incodeit.root.weatherkotlin.mapper.ImageMapper
import com.incodeit.root.weatherkotlin.model.weather_model.WeatherUi
import com.incodeit.root.weatherkotlin.ui.base.BaseActivity
import com.incodeit.root.weatherkotlin.ui.day_fragment.DayWeatherFragment
import com.incodeit.root.weatherkotlin.ui.setting.SettingActivity
import com.incodeit.root.weatherkotlin.ui.splash.SplashActivity
import com.incodeit.root.weatherkotlin.ui.three_days.ThreeDaysFragment
import com.incodeit.root.weatherkotlin.ui.week_fragment.WeekFragment
import com.incodeit.root.weatherkotlin.utils.AppConstants
import com.incodeit.root.weatherkotlin.utils.TimeUtils.getTime
import timber.log.Timber
import java.util.ArrayList
import javax.inject.Inject

class WeatherActivity : BaseActivity() , IWeatherView{

    @BindView(R.id.cities_name)
    @Nullable
    @JvmField var citiesNameTextView: TextView? = null

    @BindView(R.id.temperature)
    @Nullable @JvmField var temperatureTextView: TextView? = null

    @BindView(R.id.clouds)
    @Nullable @JvmField var cloudsTextView: TextView? = null

    @BindView(R.id.cloudsIcon)
    @Nullable @JvmField var cloudsImageView: ImageView? = null

    @BindView(R.id.banerView)
    @Nullable @JvmField var baner: ImageView? = null

    @BindView(R.id.more_setting)
    @Nullable @JvmField var moreImageButton: ImageButton? = null

    @BindView(R.id.card)
    @Nullable @JvmField var weatherCardView: CardView? = null

    @Inject
    lateinit var presenter : WeatherPresenter<IWeatherView>

    private lateinit var typeface: Typeface

    private var backPressed: Long = 0

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context , WeatherActivity::class.java)
        }

        var pref = ArrayList<String>()
    }

    override fun showDayFragment() {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.weather_frame, DayWeatherFragment.newInstance(), DayWeatherFragment.TAG)
                .commit()
    }

    override fun showWeekFragment() {
        ParentViewHolder.position = 0
        supportFragmentManager
                .beginTransaction()
                .disallowAddToBackStack()
                .replace(R.id.weather_frame, WeekFragment.newInstance(), WeekFragment.TAG)
                .commit()
    }

    override fun onError(resId: Int) {
        Timber.d("onError %s" , getString(resId))
    }

    override fun onError(errorString: String) {
        Timber.d("onError %s" , errorString)
    }

    override fun showThreeDayFragment() {
        ParentViewHolder.position = 0
        supportFragmentManager
                .beginTransaction()
                .disallowAddToBackStack()
                .replace(R.id.weather_frame, ThreeDaysFragment.newInstance(), ThreeDaysFragment.TAG)
                .commit()
    }

    @SuppressLint("SetTextI18n")
    override fun initComponent() {
        val imageMapper = ImageMapper
        val fontPath = "fonts/CenturyGothicRegular.ttf"
        typeface = Typeface.createFromAsset(assets, fontPath)
        citiesNameTextView?.text = SplashActivity.pref!![3]
        temperatureTextView?.typeface = typeface
        cloudsTextView?.typeface = typeface
        if (pref[0] == "c")
            temperatureTextView?.text = WeatherUi.temperature + "°" + AppConstants.CELSIUS_DATA
        else
            temperatureTextView?.text = ConverterParameters.getFar(WeatherUi.temperature.toString()).toString() + "°" + AppConstants.FAHRENHEIT_DATA
        val conditionMapper = ConditionMapper(baseContext)
        cloudsTextView?.text = conditionMapper.setCondition(WeatherUi.icon.toString())
        cloudsImageView?.setImageResource(imageMapper.setImage(WeatherUi.icon.toString()))
        when (pref[1]) {
            AppConstants.DAY -> showDayFragment()
            AppConstants.THREE_DAY -> showThreeDayFragment()
            AppConstants.WEEK -> showWeekFragment()
        }
        if (getTime() in 7..18) {
            baner?.setImageResource(R.drawable.header_day)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(baseContext , R.color.dayPrimary)
            }
        } else {
            baner?.setImageResource(R.drawable.header_night)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = window
                weatherCardView?.setCardBackgroundColor(Color.parseColor("#274356"))
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(baseContext , R.color.colorPrimary)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.setting , menu)
        return true
    }

    override fun networkConnected(): Boolean {
        return false
    }

    @OnClick(R.id.more_setting)
    internal fun onMoreSetting(v: View) {
        val intent = SettingActivity.getStartIntent(this@WeatherActivity)
        startActivity(intent)
        finish()
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (count.equals(0)) {
            if (backPressed + 2000 > System.currentTimeMillis()) {
                finish()
                moveTaskToBack(true)
            } else {
                Toast.makeText(baseContext , R.string.press_once , Toast.LENGTH_SHORT).show()
                backPressed = System.currentTimeMillis()
            }
        } else {
            supportFragmentManager.popBackStack()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getActivityComponents().inject(this)
        setUnBinder(ButterKnife.bind(this))
        presenter.onAttach(this)
        pref.clear()
        pref = presenter.getUserPreference(this)
        initComponent()
    }

}