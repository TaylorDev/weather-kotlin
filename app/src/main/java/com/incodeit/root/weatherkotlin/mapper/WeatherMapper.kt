package com.incodeit.root.weatherkotlin.mapper

import com.incodeit.root.weatherkotlin.model.weather_model.WeatherData
import com.incodeit.root.weatherkotlin.model.weather_model.WeatherUi

object WeatherMapper {

    var weatherUi = WeatherUi.Companion

    fun map(weatherData: WeatherData): WeatherUi.Companion {
        val main = weatherData.main
        val weatherList = weatherData.weather
        weatherUi.temperature = main!!.getTemp()?.toInt().toString()
        val weather = weatherList!![0]
        weatherUi.clouds = weather.getMain()
        weatherUi.icon = weather.getIcon()
        weatherUi.pressure = main.getPressure()?.toInt().toString()
        weatherUi.humidity = main.getHumidity()?.toInt().toString()
        val wind = weatherData.wind
        weatherUi.wind = wind!!.getSpeed().toString()
        val clouds = weatherData.clouds
        weatherUi.cloudiness = clouds!!.getAll()!!.toInt().toString()

        return weatherUi
    }

}