package com.incodeit.root.weatherkotlin.data

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.*
import android.support.v4.app.ActivityCompat
import com.incodeit.root.weatherkotlin.mapper.DailyMapper
import com.incodeit.root.weatherkotlin.mapper.ForecastMapper
import com.incodeit.root.weatherkotlin.mapper.WeatherMapper
import com.incodeit.root.weatherkotlin.model.daily_model.DailyModelUi
import com.incodeit.root.weatherkotlin.model.forecast_model.ForecastUi
import com.incodeit.root.weatherkotlin.model.weather_model.WeatherUi
import com.incodeit.root.weatherkotlin.utils.AppConstants
import io.reactivex.Observable
import timber.log.Timber
import java.io.IOException
import java.util.*
import javax.inject.Inject

class DataManager @Inject constructor(retrofit: WeatherApi) : IDataManager {

    private var apiService: WeatherApi? = retrofit
    private var defaultCity: String = AppConstants.DEFAULT_CITY
    private var context: Context? = null

    private val temperature = AppConstants.TEMPERATURE_PREF
    private val frequency = AppConstants.FREQUENCY_PREF
    private val speed = AppConstants.SPEED_PREF
    private val city = AppConstants.CITY_PREF
    private val region = AppConstants.REGION_PREF

    override fun getDataWeatherNow(city: String): Observable<WeatherUi.Companion> {
        return apiService!!.getDataWeather(city , AppConstants.UNITS , AppConstants.LANG , AppConstants.APP_ID)
                .map({ weatherData -> WeatherMapper.map(weatherData) })
    }

    override fun getDataWeatherDaily(city: String): Observable<DailyModelUi.Companion> {
        return apiService!!.getDataForecastDaily(city , AppConstants.UNITS , AppConstants.LANG , AppConstants.APP_ID)
                .map { dailyData -> DailyMapper.map(dailyData) }
    }

    override fun getDataWeatherForecast(city: String): Observable<ForecastUi.Companion> {
        return apiService!!.getDataForecast(city , AppConstants.UNITS , AppConstants.LANG , AppConstants.APP_ID)
                .map({ forecastData -> ForecastMapper.map(forecastData) })
    }

    override fun getSharePreference(activity: Activity): ArrayList<String> {
        context = activity
        val userPreference = ArrayList<String>()
        val sharedPref = activity.getSharedPreferences(AppConstants.SETTING_USER , Context.MODE_PRIVATE)
        val defaultTemperature = AppConstants.CELSIUS
        val defaultFrequency = AppConstants.DAY
        val defaultSpeed = AppConstants.SPEED
        val temperature = sharedPref.getString(temperature , defaultTemperature)
        val frequency = sharedPref.getString(frequency , defaultFrequency)
        val speed = sharedPref.getString(speed , defaultSpeed)
        val city = sharedPref.getString(city , defaultCity)
        val region = sharedPref.getString(region , defaultCity)
        getLocation(activity)
        userPreference.add(temperature)
        userPreference.add(frequency)
        userPreference.add(speed)
        userPreference.add(city)
        userPreference.add(region)
        return userPreference
    }

    @SuppressLint("MissingPermission")
    private fun getLocation(context: Activity) {
        val locationManager = context.getSystemService(LOCATION_SERVICE) as LocationManager
        val criteria = Criteria()
        locationManager.getBestProvider(criteria , true)
        if (ActivityCompat.checkSelfPermission(context , Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context , Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            try {
                val location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria , true))
                val gcd = Geocoder(context , Locale.getDefault())
                Timber.d("Tag" , "1")
                val addresses: List<Address>

                try {
                    addresses = gcd.getFromLocation(location.latitude , location.longitude , 1)
                    if (addresses.isNotEmpty()) {
                        defaultCity = addresses[0].locality.toString()
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            } catch (ex: Exception) {
                defaultCity = AppConstants.DEFAULT_CITY
            }

        } else {

            try {
                val location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria , true))
                val gcd = Geocoder(context , Locale.getDefault())
                Timber.d("Tag" , "1")
                val addresses: List<Address>

                try {
                    addresses = gcd.getFromLocation(location.latitude , location.longitude , 1)
                    if (addresses.isNotEmpty()) {
                        defaultCity = addresses[0].locality.toString()
                    }

                } catch (e: IOException) {
                    e.printStackTrace()
                }

            } catch (ex: Exception) {
                defaultCity = AppConstants.DEFAULT_CITY
            }

        }
    }

    override fun setUserCity(context: Activity , city: String) {
        val sharedPref = context.getSharedPreferences(AppConstants.SETTING_USER , Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(this.city , city)
        editor.apply()
    }

    override fun setUserRegion(context: Activity , region: String) {
        val sharedPref = context.getSharedPreferences(AppConstants.SETTING_USER , Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(this.region , region)
        editor.apply()
    }

    override fun setUserSpeed(context: Activity , speed: String) {
        val sharedPref = context.getSharedPreferences(AppConstants.SETTING_USER , Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(this.speed , speed)
        editor.apply()
    }

    override fun setUserTemperature(activity: Activity , temperature: String) {
        val sharedPref = activity.getSharedPreferences(AppConstants.SETTING_USER , Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(this.temperature , temperature)
        editor.apply()
    }

    override fun setUserFrequency(context: Activity , frequency: String) {
        val sharedPref = context.getSharedPreferences(AppConstants.SETTING_USER , Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(this.frequency , frequency)
        editor.apply()
    }

}