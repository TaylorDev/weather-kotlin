package com.incodeit.root.weatherkotlin.ui.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import butterknife.ButterKnife
import com.incodeit.root.weatherkotlin.R
import com.incodeit.root.weatherkotlin.model.daily_model.DailyModelUi
import com.incodeit.root.weatherkotlin.model.forecast_model.ForecastUi
import com.incodeit.root.weatherkotlin.ui.base.BaseActivity
import com.incodeit.root.weatherkotlin.ui.weather.WeatherActivity
import javax.inject.Inject


class SplashActivity : BaseActivity() , ISplashView {

    override fun successDaily(dailyModelUi: DailyModelUi.Companion) {
        val intent = WeatherActivity.getStartIntent(this@SplashActivity)
        startActivity(intent)
        finish()
    }

    override fun successForecast(forecastUi: ForecastUi.Companion) {
        presenter.getWeatherForecastDaily(pref?.get(4)!!)
    }

    companion object {
        var pref: ArrayList<String>? = null

        fun getStartIntent(context : Context) : Intent {
            return Intent(context, SplashActivity :: class.java)
        }

    }

    @Inject
    lateinit var presenter : SplashPresenter<ISplashView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        getActivityComponents().inject(this)
        setUnBinder(ButterKnife.bind(this))
        pref?.clear()
        pref = presenter.getUserPreference(this)
        presenter.onAttach(this)
    }

}