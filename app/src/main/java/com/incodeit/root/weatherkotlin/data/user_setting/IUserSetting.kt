package com.incodeit.root.weatherkotlin.data.user_setting

import android.app.Activity

interface IUserSetting {

    fun getSharePreference(activity : Activity) : ArrayList<String>

    fun setUserTemperature(activity: Activity, temperature : String)

    fun setUserFrequency(context: Activity, frequency: String)

    fun setUserSpeed(context: Activity, speed: String)

    fun setUserCity(context: Activity, city: String)

    fun setUserRegion(context: Activity, region: String)

}