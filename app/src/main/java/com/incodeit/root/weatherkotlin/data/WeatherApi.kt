package com.incodeit.root.weatherkotlin.data

import com.incodeit.root.weatherkotlin.model.weather_model.WeatherData
import com.incodeit.root.weatherkotlin.utils.AppConstants
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import com.incodeit.root.weatherkotlin.model.daily_model.DailyModel
import com.incodeit.root.weatherkotlin.model.forecast_model.ForecastData

interface WeatherApi {

    @GET(AppConstants.QUERY_WEATHER)
    fun getDataWeather(@Query(AppConstants.CITY_NAME) resourceName: String,
                       @Query(AppConstants.UNITS_QUERY) units: String,
                       @Query(AppConstants.LANG_QUERY) lang: String,
                       @Query(AppConstants.APP_ID_QUERY) appId: String): Observable<WeatherData>

    @GET(AppConstants.QUERY_FORECAST)
    fun getDataForecast(@Query(AppConstants.CITY_NAME) cityName: String ,
                        @Query(AppConstants.UNITS_QUERY) units: String ,
                        @Query(AppConstants.LANG_QUERY) lang: String ,
                        @Query(AppConstants.APP_ID_QUERY) appId: String): Observable<ForecastData>

    @GET(AppConstants.QUERY_FORECAST_DAILY)
    fun getDataForecastDaily(@Query(AppConstants.CITY_NAME) cityName: String ,
                             @Query(AppConstants.UNITS_QUERY) units: String ,
                             @Query(AppConstants.LANG_QUERY) lang: String ,
                             @Query(AppConstants.APP_ID_QUERY) appId: String): Observable<DailyModel>

}