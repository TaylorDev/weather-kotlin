package com.incodeit.root.weatherkotlin.model.forecast_model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlin.collections.List

class List {

    @SerializedName("main")
    @Expose
    var main: Main? = null

    @SerializedName("weather")
    @Expose
    var weather: List<Weather>? = null

    @SerializedName("clouds")
    @Expose
    var wind: Wind? = null

    @SerializedName("dt_txt")
    @Expose
    var dtTxt: String? = null

}