package com.incodeit.root.weatherkotlin.ui.day_fragment

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Bundle
import android.support.annotation.Nullable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.incodeit.root.weatherkotlin.R
import com.incodeit.root.weatherkotlin.data.ConverterParameters
import com.incodeit.root.weatherkotlin.model.weather_model.WeatherUi
import com.incodeit.root.weatherkotlin.ui.base.BaseFragment
import com.incodeit.root.weatherkotlin.ui.splash.SplashActivity
import com.incodeit.root.weatherkotlin.utils.AppConstants
import java.text.DecimalFormat
import javax.inject.Inject

class DayWeatherFragment : BaseFragment(), IDayWeatherView {

    private var typeface: Typeface? = null


    @BindView(R.id.pressure_data)
    @Nullable @JvmField var pressureTextView: TextView? = null

    @BindView(R.id.humidity_data)
    @Nullable @JvmField var humidityTextView: TextView? = null

    @BindView(R.id.wind_data)
    @Nullable @JvmField var windTextView: TextView? = null

    @BindView(R.id.cloudiness_data)
    @Nullable @JvmField var cloudinessTextView: TextView? = null

    @Inject
    lateinit var presenter: DayWeatherPresenter<IDayWeatherView>

    companion object {
        val TAG = "DayWeatherFragment"

        fun newInstance(): DayWeatherFragment {
            val args = Bundle()
            val fragment = DayWeatherFragment()
            fragment.arguments = args
            return fragment
        }

    }

    override fun onCreateView(inflater: LayoutInflater? , container: ViewGroup? ,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_day , container , false)
        val component = getActivityComponent()
        component?.inject(this)
        unBinder = ButterKnife.bind(this , view)
        presenter.onAttach(this)
        val fontPath = "fonts/CenturyGothicRegular.ttf"
        typeface = Typeface.createFromAsset(activity.assets , fontPath)
        initComp()
        return view
    }

    override fun networkConnected(): Boolean {
        return false
    }

    @SuppressLint("SetTextI18n")
    private fun initComp() {
        pressureTextView?.typeface = typeface
        humidityTextView?.typeface = typeface
        windTextView?.typeface = typeface
        cloudinessTextView?.typeface = typeface
        pressureTextView?.text = WeatherUi.Companion.pressure + AppConstants.PRESSURE
        humidityTextView?.text = WeatherUi.humidity + AppConstants.PERCENT
        if (SplashActivity.pref?.get(2).equals(AppConstants.SPEED)) {
            windTextView?.text = WeatherUi.wind + " " + getActivity().getString(R.string.km)
        } else {
            windTextView?.text = DecimalFormat("#0.00").format(ConverterParameters.getMiles(WeatherUi.Companion.wind.toString())) + " " + getActivity().getString(R.string.mi)
        }
        cloudinessTextView?.text = WeatherUi.cloudiness + AppConstants.PERCENT
    }

}