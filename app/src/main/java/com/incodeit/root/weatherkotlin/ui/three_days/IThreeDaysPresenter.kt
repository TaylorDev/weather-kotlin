package com.incodeit.root.weatherkotlin.ui.three_days

import com.incodeit.root.weatherkotlin.adapter.Weather.DailyWeatherRecyclerAdapter
import com.incodeit.root.weatherkotlin.di.ActivityScope
import com.incodeit.root.weatherkotlin.model.daily_model.Daily
import com.incodeit.root.weatherkotlin.ui.base.IBasePresenter

@ActivityScope
interface IThreeDaysPresenter<in V : IThreeDaysView> : IBasePresenter<V> {

    fun initArray() : ArrayList<Daily>

    fun getAdapter() : DailyWeatherRecyclerAdapter

}