package com.incodeit.root.weatherkotlin.ui.three_days

import com.incodeit.root.weatherkotlin.adapter.Weather.DailyWeatherRecyclerAdapter
import com.incodeit.root.weatherkotlin.data.IDataManager
import com.incodeit.root.weatherkotlin.model.daily_model.Daily
import com.incodeit.root.weatherkotlin.model.daily_model.DailyModelUi
import io.reactivex.disposables.CompositeDisposable
import com.incodeit.root.weatherkotlin.utils.rx.ISchedulerProvider
import javax.inject.Inject
import com.incodeit.root.weatherkotlin.ui.base.BasePresenter

class ThreeDaysPresenter<V : IThreeDaysView> @Inject
constructor(schedulerProvider: ISchedulerProvider,
            compositeDisposable: CompositeDisposable,
            dataManager: IDataManager)
        : BasePresenter<V>(schedulerProvider , compositeDisposable , dataManager) , IThreeDaysPresenter<V> {

        private var arrayListThreeDay = ArrayList<Daily>()
        private lateinit var adapter: DailyWeatherRecyclerAdapter

        override fun initArray(): ArrayList<Daily> {
                arrayListThreeDay = ArrayList()
                for (i in 0..2) {
                        arrayListThreeDay.add(DailyModelUi.Companion.listDaily!![i])
                }
                return arrayListThreeDay
        }

        override fun getAdapter(): DailyWeatherRecyclerAdapter {
                adapter = DailyWeatherRecyclerAdapter()
                adapter.addAll(initArray())
                return adapter
        }

}