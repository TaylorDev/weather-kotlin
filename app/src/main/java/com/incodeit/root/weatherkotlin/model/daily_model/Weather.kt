package com.incodeit.root.weatherkotlin.model.daily_model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Weather {

    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("main")
    @Expose
    var main: String? = null

    @SerializedName("icon")
    @Expose
    var icon: String? = null

}