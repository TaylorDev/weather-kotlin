package com.incodeit.root.weatherkotlin.model.daily_model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlin.collections.List

class List {

    @SerializedName("dt")
    @Expose
    var dt: Double? = null

    @SerializedName("temp")
    @Expose
    var temp: Temp? = null

    @SerializedName("pressure")
    @Expose
    var pressure: Double? = null

    @SerializedName("humidity")
    @Expose
    var humidity: Double? = null

    @SerializedName("weather")
    @Expose
    var weather: List<Weather>? = null

    @SerializedName("speed")
    @Expose
    var speed: Double? = null

    @SerializedName("clouds")
    @Expose
    var clouds: Double? = null

}