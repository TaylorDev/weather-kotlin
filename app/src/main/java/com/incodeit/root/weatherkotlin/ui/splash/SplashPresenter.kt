package com.incodeit.root.weatherkotlin.ui.splash

import android.app.Activity
import com.incodeit.root.weatherkotlin.data.IDataManager
import com.incodeit.root.weatherkotlin.mapper.DailyMapper.dailyModelUi
import com.incodeit.root.weatherkotlin.mapper.ForecastMapper.forecastUi
import com.incodeit.root.weatherkotlin.mapper.WeatherMapper.weatherUi
import com.incodeit.root.weatherkotlin.ui.base.BasePresenter
import com.incodeit.root.weatherkotlin.utils.rx.ISchedulerProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class SplashPresenter <V : ISplashView> @Inject
constructor(schedulerProvider: ISchedulerProvider ,
            compositeDisposable: CompositeDisposable ,
            dataManager: IDataManager) : BasePresenter<V>(schedulerProvider , compositeDisposable , dataManager) , ISplashPresenter<V> {

    override fun getWeatherForecast(city: String) {
        dataManager.getDataWeatherForecast(city)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ forecastData ->
                    forecastUi = forecastData
                    mvpView!!.successForecast(forecastUi)
                })
    }

    override fun getWeatherForecastDaily(city: String) {
        dataManager.getDataWeatherDaily(city)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ daily ->
                    dailyModelUi = daily
                    mvpView!!.successDaily(dailyModelUi)
                })
    }

    override fun getWeatherNow(city: String) {
        dataManager.getDataWeatherNow(city)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ weatherData ->
                    weatherUi = weatherData
                })
    }

    override fun getUserPreference(activity: Activity): ArrayList<String> {
       return dataManager.getSharePreference(activity)
    }

    override fun onAttach(view: V) {
        super.onAttach(view)
        getWeatherNow(SplashActivity.pref!![4])
        getWeatherForecast(SplashActivity.pref!![4])

    }

}