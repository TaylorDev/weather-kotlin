package com.incodeit.root.weatherkotlin.model.forecast_model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Main {

    @SerializedName("temp")
    @Expose
    var temp: Double? = null

    @SerializedName("pressure")
    @Expose
    var pressure: Double? = null

}