package com.incodeit.root.weatherkotlin.ui.hour_weather_fragment

import com.incodeit.root.weatherkotlin.adapter.HourWeather.HourWeatherRecyclerAdapter
import com.incodeit.root.weatherkotlin.model.daily_model.Daily
import com.incodeit.root.weatherkotlin.ui.base.IBasePresenter

interface IHourWeatherPresenter<in V : IHourWeatherView> : IBasePresenter<V>{

    fun getDaily() : Daily

    fun getHourAdapter() : HourWeatherRecyclerAdapter

    fun getWeatherDaily()
}