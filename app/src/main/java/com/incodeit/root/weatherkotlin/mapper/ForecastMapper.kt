package com.incodeit.root.weatherkotlin.mapper

import com.incodeit.root.weatherkotlin.model.forecast_model.Forecast
import com.incodeit.root.weatherkotlin.model.forecast_model.ForecastData
import com.incodeit.root.weatherkotlin.model.forecast_model.ForecastUi

object ForecastMapper {
    var forecastUi = ForecastUi.Companion
    private var forecast: Forecast? = null

    fun map(weatherData: ForecastData): ForecastUi.Companion {
        val liefest = weatherData.list
        forecastUi.listForecast = ArrayList()
        for (list in liefest!!) {
            forecast = Forecast()
            forecast!!.dateForecast = list.dtTxt
            val weather = list.weather?.get(0)
            forecast!!.cloudsForecast = weather?.main
            forecast!!.iconForecast = weather?.icon
            forecast!!.dateForecast = list.dtTxt
            val main = list.main
            forecast!!.temperatureForecast = main?.temp?.toInt().toString()
            forecastUi.listForecast!!.add(forecast!!)

        }
        return forecastUi
    }
}