package com.incodeit.root.weatherkotlin.ui.setting

import android.app.Activity
import com.incodeit.root.weatherkotlin.data.IDataManager
import com.incodeit.root.weatherkotlin.di.ActivityScope
import com.incodeit.root.weatherkotlin.ui.base.BasePresenter
import com.incodeit.root.weatherkotlin.utils.rx.ISchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


@ActivityScope
class SettingPresenter <V : ISettingView> @Inject
constructor(schedulerProvider: ISchedulerProvider ,
            compositeDisposable: CompositeDisposable ,
            dataManager: IDataManager) : BasePresenter<V>(schedulerProvider , compositeDisposable , dataManager) , ISettingPresenter<V>{

    override fun setUserFrequency(activity: Activity , frequency: String) {
        dataManager.setUserFrequency(activity , frequency)
    }

    override fun setUserTemperature(activity: Activity , temperature: String) {
        dataManager.setUserTemperature(activity , temperature)
    }

    override fun setUserSpeed(activity: Activity , speed: String) {
        dataManager.setUserSpeed(activity , speed)
    }

    override fun setUserCity(activity: Activity , city: String) {
        dataManager.setUserCity(activity , city)
    }

    override fun setUserRegion(activity: Activity , region: String) {
        dataManager.setUserRegion(activity , region)
    }

}