package com.incodeit.root.weatherkotlin.model.forecast_model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ForecastData {

    @SerializedName("list")
    @Expose
    var list: kotlin.collections.List<List>? = null

    @SerializedName("city")
    @Expose
    var city: City? = null

}