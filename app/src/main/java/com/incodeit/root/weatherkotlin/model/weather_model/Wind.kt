package com.incodeit.root.weatherkotlin.model.weather_model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Wind {

    @SerializedName("speed")
    @Expose
    private val speed : Double? = null

    fun getSpeed() : Double? {
        return this.speed
    }

}