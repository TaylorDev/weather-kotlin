package com.incodeit.root.weatherkotlin.adapter.Weather

enum class RecyclerItemType(val value: Int) {
    NORMAL(0) , TYPE1(1) , TYPE2(2) , TYPE3(3) , PARENT(4) , CHILD(5)
}