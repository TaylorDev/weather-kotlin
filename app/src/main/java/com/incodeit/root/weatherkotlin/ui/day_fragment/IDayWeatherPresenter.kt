package com.incodeit.root.weatherkotlin.ui.day_fragment

import com.incodeit.root.weatherkotlin.di.ActivityScope
import com.incodeit.root.weatherkotlin.ui.base.IBasePresenter

@ActivityScope
interface IDayWeatherPresenter<in V : IDayWeatherView> : IBasePresenter<V>