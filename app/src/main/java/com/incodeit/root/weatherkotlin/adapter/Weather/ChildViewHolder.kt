package com.incodeit.root.weatherkotlin.adapter.Weather

import android.annotation.SuppressLint
import android.content.Context
import android.view.ViewGroup
import android.widget.TextView


import com.incodeit.root.weatherkotlin.R
import com.incodeit.root.weatherkotlin.model.daily_model.WeatherDailyParameter

import butterknife.BindView
import butterknife.ButterKnife
import com.incodeit.root.weatherkotlin.utils.AppConstants
import zlc.season.practicalrecyclerview.AbstractViewHolder

class ChildViewHolder internal constructor(parent: ViewGroup) : AbstractViewHolder<WeatherDailyParameter>(parent , R.layout.list_item_recycler_child) {

    @BindView(R.id.pressure_data_daily)
    @JvmField
    internal var pressureDataDaily: TextView? = null

    @BindView(R.id.humidity_data_daily)
    @JvmField
    internal var humidityDataDaily: TextView? = null

    @BindView(R.id.wind_data_daily)
    @JvmField
    internal var windDataDaily: TextView? = null

    @BindView(R.id.cloudiness_data_daily)
    @JvmField
    internal var cloudinessDataDaily: TextView? = null

    private val mContext: Context

    init {
        ButterKnife.bind(this , itemView)
        mContext = parent.context
    }

    @SuppressLint("SetTextI18n")
    override fun setData(data: WeatherDailyParameter) {
        pressureDataDaily!!.text = data.pressureDaily!! + AppConstants.PRESSURE
        humidityDataDaily!!.text = data.humidityDaily!! + AppConstants.PERCENT
        windDataDaily!!.text = data.windDaily!! + mContext.getString(R.string.km)
        cloudinessDataDaily!!.text = data.cloudinessDaily!! + AppConstants.PERCENT
    }

}