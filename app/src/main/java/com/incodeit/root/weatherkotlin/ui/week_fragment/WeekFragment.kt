package com.incodeit.root.weatherkotlin.ui.week_fragment

import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import com.incodeit.root.weatherkotlin.R
import com.incodeit.root.weatherkotlin.adapter.Weather.DailyWeatherRecyclerAdapter
import com.incodeit.root.weatherkotlin.model.daily_model.DailyModelUi
import com.incodeit.root.weatherkotlin.ui.base.BaseFragment
import zlc.season.practicalrecyclerview.PracticalRecyclerView
import javax.annotation.Nullable
import javax.inject.Inject

class WeekFragment : BaseFragment(), IWeekView {

    private var typeface: Typeface? = null

    @Inject
    lateinit var presenter: WeekPresenter<IWeekView>

    @BindView(R.id.recyclerViewWeek)
    @Nullable @JvmField var recyclerView: PracticalRecyclerView? = null

    companion object {

        val TAG = "WeekFragment"

        fun newInstance() : WeekFragment{
            val args = Bundle()
            val fragment = WeekFragment()
            fragment.arguments = args
            return fragment
        }

    }

    override fun onCreateView(inflater: LayoutInflater? , container: ViewGroup? ,
                              savedInstanceState: Bundle?): View? {
        val view  = inflater!!.inflate(R.layout.fragment_week , container , false)
        val components = getActivityComponent()
        if (components != null){
            components.inject(this)
            unBinder = ButterKnife.bind(this, view)
            presenter.onAttach(this)
        }
        val fontPath = "fonts/CenturyGothicRegular.ttf"
        typeface = Typeface.createFromAsset(activity.assets, fontPath)

        recyclerView?.overScrollMode = 2
        recyclerView?.setLayoutManager(LinearLayoutManager(view.context))
        recyclerView?.setAdapter(presenter.initAdapter())
        return view
    }

    override fun networkConnected(): Boolean {
        return false
    }

}