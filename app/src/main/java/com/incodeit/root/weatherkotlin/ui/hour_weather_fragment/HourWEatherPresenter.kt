package com.incodeit.root.weatherkotlin.ui.hour_weather_fragment

import com.incodeit.root.weatherkotlin.adapter.HourWeather.HourWeatherRecyclerAdapter
import com.incodeit.root.weatherkotlin.adapter.Weather.ParentViewHolder
import com.incodeit.root.weatherkotlin.data.IDataManager
import com.incodeit.root.weatherkotlin.model.daily_model.Daily
import com.incodeit.root.weatherkotlin.model.daily_model.DailyModelUi
import com.incodeit.root.weatherkotlin.model.daily_model.WeatherDailyParameter
import io.reactivex.disposables.CompositeDisposable
import com.incodeit.root.weatherkotlin.utils.rx.ISchedulerProvider
import javax.inject.Inject
import com.incodeit.root.weatherkotlin.ui.base.BasePresenter

class HourWeatherPresenter<V : IHourWeatherView> @Inject
constructor(schedulerProvider: ISchedulerProvider,
            compositeDisposable: CompositeDisposable,
            dataManager: IDataManager)
    : BasePresenter<V>(schedulerProvider , compositeDisposable , dataManager) , IHourWeatherPresenter<V>{

    private var daily = Daily()
    var weatherDailyParameter = WeatherDailyParameter()

    override fun getDaily() : Daily{
        daily = DailyModelUi.Companion.listDaily!![ParentViewHolder.Companion.position]
        getWeatherDaily()
        return daily
    }

    override fun getHourAdapter() : HourWeatherRecyclerAdapter{
        return HourWeatherRecyclerAdapter(daily.forecastParameters!!)
    }

    override fun getWeatherDaily() {
        weatherDailyParameter = daily.dailyParameters!![0]
    }

}