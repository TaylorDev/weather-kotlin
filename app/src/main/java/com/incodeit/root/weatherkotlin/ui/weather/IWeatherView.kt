package com.incodeit.root.weatherkotlin.ui.weather

import com.incodeit.root.weatherkotlin.ui.base.IBaseView

interface IWeatherView : IBaseView {

    fun showDayFragment()

    fun showWeekFragment()

    fun showThreeDayFragment()

    fun initComponent()
}