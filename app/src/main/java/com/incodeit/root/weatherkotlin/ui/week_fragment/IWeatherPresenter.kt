package com.incodeit.root.weatherkotlin.ui.week_fragment

import com.incodeit.root.weatherkotlin.adapter.Weather.DailyWeatherRecyclerAdapter
import com.incodeit.root.weatherkotlin.di.ActivityScope
import com.incodeit.root.weatherkotlin.ui.base.IBasePresenter

@ActivityScope
interface IWeekPresenter<in V : IWeekView> : IBasePresenter<V> {

    fun initAdapter() : DailyWeatherRecyclerAdapter

}