package com.incodeit.root.weatherkotlin.model.weather_model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class WeatherData {

    @SerializedName("weather")
    @Expose
    var weather: List<Weather>? = null

    @SerializedName("base")
    @Expose
    var base: String? = null

    @SerializedName("main")
    @Expose
    var main: Main? = null

    @SerializedName("wind")
    @Expose
    var wind: Wind? = null

    @SerializedName("clouds")
    @Expose
    var clouds: Clouds? = null

    @SerializedName("id")
    @Expose
    var id: Double? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

}