package com.incodeit.root.weatherkotlin.mapper

import android.content.Context
import com.incodeit.root.weatherkotlin.R


data class ConditionMapper(private val context: Context) {

    private var conditionWeather: String? = null

    fun setCondition(clouds: String): String? {
        when (clouds) {
            "01d" -> conditionWeather = context.getString(R.string.clear)
            "01n" -> conditionWeather = context.getString(R.string.clear)
            "02d" -> conditionWeather = context.getString(R.string.clouds)
            "02n" -> conditionWeather = context.getString(R.string.clouds)
            "03d" -> conditionWeather = context.getString(R.string.clouds)
            "03n" -> conditionWeather = context.getString(R.string.clouds)
            "04d" -> conditionWeather = context.getString(R.string.clouds)
            "04n" -> conditionWeather = context.getString(R.string.clouds)
            "09d" -> conditionWeather = context.getString(R.string.rain)
            "09n" -> conditionWeather = context.getString(R.string.rain)
            "10d" -> conditionWeather = context.getString(R.string.rain)
            "10n" -> conditionWeather = context.getString(R.string.rain)
            "11d" -> conditionWeather = context.getString(R.string.thunderstorm)
            "11n" -> conditionWeather = context.getString(R.string.thunderstorm)
            "13d" -> conditionWeather = context.getString(R.string.snow)
            "13n" -> conditionWeather = context.getString(R.string.snow)
            "50d" -> conditionWeather = context.getString(R.string.mist)
            "50n" -> conditionWeather = context.getString(R.string.mist)
        }
        return conditionWeather
    }
}