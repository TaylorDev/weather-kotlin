package com.incodeit.root.weatherkotlin.adapter.HourWeather

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import com.incodeit.root.weatherkotlin.R

class HourWeatherHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var weatherImage: ImageView = itemView.findViewById<View>(R.id.item_image) as ImageView
    var weatherTemp: TextView = itemView.findViewById<View>(R.id.item_temp) as TextView
    var weatherDescription: TextView = itemView.findViewById<View>(R.id.item_description) as TextView
    var weatherCard: CardView = itemView.findViewById<View>(R.id.weather_card) as CardView

}