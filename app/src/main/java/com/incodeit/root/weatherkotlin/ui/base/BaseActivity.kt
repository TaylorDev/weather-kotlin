package com.incodeit.root.weatherkotlin.ui.base

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.incodeit.root.weatherkotlin.di.component.ActivityComponent
import com.incodeit.root.weatherkotlin.utils.NetworkUtils
import butterknife.Unbinder
import com.incodeit.root.weatherkotlin.app.WeatherApplication
import com.incodeit.root.weatherkotlin.di.module.ActivityModule
import com.incodeit.root.weatherkotlin.di.component.DaggerActivityComponent
import timber.log.Timber
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity(), IBaseView {

    private lateinit var activityComponent : ActivityComponent

    private lateinit var mUnBinder: Unbinder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent = DaggerActivityComponent.builder()
                .activityModule(ActivityModule(this))
                .applicationComponent((application as WeatherApplication).getComponent())
                .build()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    fun getActivityComponents(): ActivityComponent {
        return activityComponent
    }

    fun setUnBinder(unBinder: Unbinder) {
        mUnBinder = unBinder
    }

    override fun networkConnected(): Boolean {
        return NetworkUtils.networkConnected(applicationContext)
    }

    fun onFragmentAttached() {

    }

    override fun onError(resId: Int) {
        Timber.d("onError %s", getString(resId))
    }

    override fun onError(errorString: String) {
        Timber.d("onError %s", errorString)
    }

}