package com.incodeit.root.weatherkotlin.data

object ConverterParameters {

    fun getMiles(windString: String): Double? {
        return java.lang.Double.parseDouble(windString) * 0.44704
    }

    fun getFar(temp: String): Int {
        return Integer.parseInt(temp) * 9 / 5 + 32
    }
}