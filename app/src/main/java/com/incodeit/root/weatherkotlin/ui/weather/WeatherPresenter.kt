package com.incodeit.root.weatherkotlin.ui.weather

import android.app.Activity
import com.incodeit.root.weatherkotlin.data.IDataManager
import com.incodeit.root.weatherkotlin.ui.base.BasePresenter
import com.incodeit.root.weatherkotlin.utils.rx.ISchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class WeatherPresenter<V : IWeatherView> @Inject
constructor(schedulerProvider: ISchedulerProvider ,
            compositeDisposable: CompositeDisposable ,
            dataManager: IDataManager) : BasePresenter<V>(schedulerProvider , compositeDisposable , dataManager) , IWeatherPresenter<V> {

    override fun getUserPreference(activity: Activity): ArrayList<String> {
        return dataManager.getSharePreference(activity)
    }

}