package com.incodeit.root.weatherkotlin.model.daily_model

import com.incodeit.root.weatherkotlin.adapter.Weather.RecyclerItemType
import com.incodeit.root.weatherkotlin.model.forecast_model.Forecast

import java.util.ArrayList

import zlc.season.practicalrecyclerview.ItemType

class Daily : ItemType {

    var cloudsDaily: String? = null

    var temperatureDaily: String? = null

    var iconDaily: String? = null

    var dateDaily: String? = null

    var dailyParameters: ArrayList<WeatherDailyParameter>? = null

    var forecastParameters: ArrayList<Forecast>? = null

    override fun itemType(): Int {
        return RecyclerItemType.PARENT.value
    }
}