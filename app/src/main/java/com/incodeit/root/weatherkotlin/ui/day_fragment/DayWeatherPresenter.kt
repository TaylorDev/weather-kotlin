package com.incodeit.root.weatherkotlin.ui.day_fragment

import com.incodeit.root.weatherkotlin.ui.base.BasePresenter
import com.incodeit.root.weatherkotlin.data.IDataManager
import io.reactivex.disposables.CompositeDisposable
import com.incodeit.root.weatherkotlin.utils.rx.ISchedulerProvider
import javax.inject.Inject

class DayWeatherPresenter<V : IDayWeatherView>  @Inject
constructor(schedulerProvider: ISchedulerProvider ,
            compositeDisposable: CompositeDisposable ,
            dataManager: IDataManager) : BasePresenter<V>(schedulerProvider , compositeDisposable , dataManager) , IDayWeatherPresenter<V>