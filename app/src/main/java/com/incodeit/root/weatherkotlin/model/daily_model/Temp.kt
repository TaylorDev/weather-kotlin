package com.incodeit.root.weatherkotlin.model.daily_model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Temp {

    @SerializedName("day")
    @Expose
    var day: Double? = null

}