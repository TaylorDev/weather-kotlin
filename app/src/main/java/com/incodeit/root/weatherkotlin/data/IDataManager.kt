package com.incodeit.root.weatherkotlin.data

import android.app.Activity
import com.incodeit.root.weatherkotlin.data.user_setting.IUserSetting
import com.incodeit.root.weatherkotlin.model.weather_model.WeatherUi
import io.reactivex.Observable
import kotlin.collections.ArrayList
import com.incodeit.root.weatherkotlin.model.daily_model.DailyModelUi
import com.incodeit.root.weatherkotlin.model.forecast_model.ForecastUi

interface IDataManager : IUserSetting{

    fun getDataWeatherNow(city: String): Observable<WeatherUi.Companion>

    fun getDataWeatherForecast(city: String): Observable<ForecastUi.Companion>

    fun getDataWeatherDaily(city: String): Observable<DailyModelUi.Companion>

    override fun getSharePreference(activity: Activity): ArrayList<String>

}