package com.incodeit.root.weatherkotlin.model.forecast_model

class Forecast {

    var cloudsForecast: String? = null

    var temperatureForecast: String? = null

    var iconForecast: String? = null

    var dateForecast: String? = null

}