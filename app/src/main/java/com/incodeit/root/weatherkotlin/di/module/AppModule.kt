package com.incodeit.root.weatherkotlin.di.module

import android.content.Context
import com.incodeit.root.weatherkotlin.R
import com.incodeit.root.weatherkotlin.data.IDataManager
import com.incodeit.root.weatherkotlin.data.WeatherApi
import com.incodeit.root.weatherkotlin.data.DataManager
import com.incodeit.root.weatherkotlin.app.WeatherApplication
import com.incodeit.root.weatherkotlin.utils.AppConstants
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import javax.inject.Singleton

@Module
class AppModule(private val application: WeatherApplication){

    @Provides
    @Singleton
    fun provideApplicationContext() : Context = application

    @Provides
    @Singleton
    fun provideApplication() : WeatherApplication = application

    @Provides
    @Singleton
    fun provideCalligraphyDefaultConfig(): CalligraphyConfig {
        return CalligraphyConfig.Builder()
                .setDefaultFontPath("")
                .setFontAttrId(R.attr.fontPath)
                .build()
    }

    @Provides
    @Singleton
    fun provideDataManager(dataManager: DataManager) : IDataManager{
        return dataManager
    }

    @Provides
    @Singleton
    fun provideRetrofitDefaultConfig() : WeatherApi {
        val rxAdapter = RxJava2CallAdapterFactory.create()
        val json = GsonBuilder().create()
        val retrofit = Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(json))
                .addCallAdapterFactory(rxAdapter)
                .build()
        return retrofit.create(WeatherApi::class.java)
    }
}