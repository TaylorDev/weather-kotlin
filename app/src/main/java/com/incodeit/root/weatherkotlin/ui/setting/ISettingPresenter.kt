package com.incodeit.root.weatherkotlin.ui.setting

import com.incodeit.root.weatherkotlin.ui.base.IBasePresenter
import android.app.Activity

interface ISettingPresenter<in V : ISettingView> : IBasePresenter<V> {

    fun setUserFrequency(activity: Activity , frequency: String)

    fun setUserTemperature(activity: Activity , temperature: String)

    fun setUserSpeed(activity: Activity , speed: String)

    fun setUserCity(activity: Activity , city: String)

    fun setUserRegion(activity: Activity , region: String)

}