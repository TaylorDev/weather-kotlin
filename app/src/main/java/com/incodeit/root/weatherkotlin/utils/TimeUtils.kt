package com.incodeit.root.weatherkotlin.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

object TimeUtils {
    @SuppressLint("SimpleDateFormat")
    fun getTime(): Int {
        val calendar = Calendar.getInstance()
        val hoarfrost = SimpleDateFormat("HH")
        return Integer.parseInt(hoarfrost.format(calendar.time))
    }
}