@file:Suppress("IMPLICIT_BOXING_IN_IDENTITY_EQUALS")

package com.incodeit.root.weatherkotlin.mapper

import android.annotation.SuppressLint
import com.incodeit.root.weatherkotlin.model.daily_model.Daily
import com.incodeit.root.weatherkotlin.model.daily_model.DailyModel
import com.incodeit.root.weatherkotlin.model.daily_model.DailyModelUi
import com.incodeit.root.weatherkotlin.model.daily_model.WeatherDailyParameter
import com.incodeit.root.weatherkotlin.model.forecast_model.Forecast
import com.incodeit.root.weatherkotlin.model.forecast_model.ForecastUi
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DailyMapper {

    var dailyModelUi = DailyModelUi.Companion

    private var daily: Daily? = null
    private var forecast: Forecast? = null

    @SuppressLint("SimpleDateFormat")
    @Throws(ParseException::class)
    fun map(dailyData: DailyModel): DailyModelUi.Companion {
        DailyModelUi.Companion.listDaily = ArrayList()
        val dailyList = dailyData.list
        for (dailyItem in dailyList!!) {
            daily = Daily()
            val daileWeather = dailyItem.weather?.get(0)
            daily!!.cloudsDaily = daileWeather?.main
            daily!!.iconDaily = daileWeather?.icon
            val tempDaily = dailyItem.temp
            daily!!.temperatureDaily = tempDaily?.day!!.toInt().toString()
            val dv = dailyItem.dt?.toInt()!!.toString().toLong() * 1000
            val df = java.util.Date(dv)
            daily!!.dateDaily = SimpleDateFormat("EEE, MMM d, ''yy").format(df)
            daily!!.dailyParameters = ArrayList()
            daily!!.forecastParameters = ArrayList()
            val weatherDailyParameter = WeatherDailyParameter()
            weatherDailyParameter.pressureDaily = dailyItem.pressure?.toInt().toString()
            weatherDailyParameter.humidityDaily = dailyItem.humidity?.toInt().toString()
            weatherDailyParameter.windDaily = dailyItem.speed?.toString()
            weatherDailyParameter.cloudinessDaily = dailyItem.clouds?.toInt().toString()
            daily!!.dailyParameters?.add(weatherDailyParameter)
            for (item in ForecastUi.listForecast!!) {
                val t =SimpleDateFormat("yyyy-MM-dd").format(df)
                if (item.dateForecast!!.contains(t)) {
                    val date = item.dateForecast!!.substring(item.dateForecast!!.length - 8 , item.dateForecast!!.length - 3)
                        forecast = Forecast()
                        forecast!!.dateForecast = date
                        forecast!!.temperatureForecast = item.temperatureForecast
                        forecast!!.iconForecast = item.iconForecast
                        forecast!!.cloudsForecast = item.cloudsForecast
                        daily!!.forecastParameters?.add(forecast!!)
                }
            }
            if (!daily!!.forecastParameters!!.size.equals(0)) {
                dailyModelUi.listDaily!!.add(daily!!)
            }
        }
        return dailyModelUi
    }
}