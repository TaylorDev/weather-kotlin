package com.incodeit.root.weatherkotlin.di.module

import android.support.v7.app.AppCompatActivity
import com.incodeit.root.weatherkotlin.di.ActivityScope
import com.incodeit.root.weatherkotlin.ui.day_fragment.DayWeatherPresenter
import com.incodeit.root.weatherkotlin.ui.day_fragment.IDayWeatherPresenter
import com.incodeit.root.weatherkotlin.ui.day_fragment.IDayWeatherView
import com.incodeit.root.weatherkotlin.ui.hour_weather_fragment.HourWeatherPresenter
import com.incodeit.root.weatherkotlin.ui.hour_weather_fragment.IHourWeatherPresenter
import com.incodeit.root.weatherkotlin.ui.hour_weather_fragment.IHourWeatherView
import com.incodeit.root.weatherkotlin.ui.setting.ISettingPresenter
import com.incodeit.root.weatherkotlin.ui.setting.ISettingView
import com.incodeit.root.weatherkotlin.ui.setting.SettingPresenter
import com.incodeit.root.weatherkotlin.ui.splash.ISplashPresenter
import com.incodeit.root.weatherkotlin.ui.splash.ISplashView
import com.incodeit.root.weatherkotlin.ui.splash.SplashPresenter
import com.incodeit.root.weatherkotlin.ui.three_days.IThreeDaysPresenter
import com.incodeit.root.weatherkotlin.ui.three_days.IThreeDaysView
import com.incodeit.root.weatherkotlin.ui.three_days.ThreeDaysPresenter
import com.incodeit.root.weatherkotlin.ui.weather.IWeatherPresenter
import com.incodeit.root.weatherkotlin.ui.weather.IWeatherView
import com.incodeit.root.weatherkotlin.ui.weather.WeatherPresenter
import com.incodeit.root.weatherkotlin.ui.week_fragment.IWeekPresenter
import com.incodeit.root.weatherkotlin.ui.week_fragment.IWeekView
import com.incodeit.root.weatherkotlin.ui.week_fragment.WeekPresenter
import com.incodeit.root.weatherkotlin.utils.rx.AppSchedulerProvider
import com.incodeit.root.weatherkotlin.utils.rx.ISchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class ActivityModule(private val mActivity: AppCompatActivity) {

    @Provides
    @ActivityScope
    fun provideContext(): AppCompatActivity {
        return mActivity
    }

    @Provides
    @ActivityScope
    fun provideCompositeDisposable(): CompositeDisposable {
        return CompositeDisposable()
    }

    @Provides
    @ActivityScope
    fun provideSchedulerProvide(): ISchedulerProvider {
        return AppSchedulerProvider()
    }

    @Provides
    @ActivityScope
    fun provideWeatherPresenter(
            presenter: WeatherPresenter<IWeatherView>): IWeatherPresenter<IWeatherView> {
        return presenter
    }

    @Provides
    @ActivityScope
    fun provideSplashPresenter(
            presenter:SplashPresenter<ISplashView>) : ISplashPresenter<ISplashView>{
        return presenter
    }

    @Provides
    @ActivityScope
    fun provideSettingPresenter(
            presenter: SettingPresenter<ISettingView>): ISettingPresenter<ISettingView> {
        return presenter
    }

    @Provides
    @ActivityScope
    fun provideDayPresenter(
            presenter: DayWeatherPresenter<IDayWeatherView>): IDayWeatherPresenter<IDayWeatherView> {
        return presenter
    }

    @Provides
    @ActivityScope
    fun provideThreeDayPresenter(
            presenter: ThreeDaysPresenter<IThreeDaysView>): IThreeDaysPresenter<IThreeDaysView> {
        return presenter
    }

    @Provides
    @ActivityScope
    fun provideWeekPresenter(
            presenter: WeekPresenter<IWeekView>): IWeekPresenter<IWeekView> {
        return presenter
    }

    @Provides
    @ActivityScope
    fun provideHourWeatherPresenter(
            presenter: HourWeatherPresenter<IHourWeatherView>): IHourWeatherPresenter<IHourWeatherView> {
        return presenter
    }

}