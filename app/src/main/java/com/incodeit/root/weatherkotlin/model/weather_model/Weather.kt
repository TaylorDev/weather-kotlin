package com.incodeit.root.weatherkotlin.model.weather_model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Weather {

    @SerializedName("main")
    @Expose
    private var main : String? = null

    @SerializedName("icon")
    @Expose
    private var icon : String? = null

    fun getMain() : String? {
        return this.main
    }

    fun getIcon() : String?{
        return this.icon
    }

}