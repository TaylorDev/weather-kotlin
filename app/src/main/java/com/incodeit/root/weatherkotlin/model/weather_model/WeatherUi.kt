package com.incodeit.root.weatherkotlin.model.weather_model

class WeatherUi {

    companion object {
        var clouds: String? = null

        var temperature: String? = null

        var icon: String? = null

        var pressure: String? = null

        var humidity: String? = null

        var wind: String? = null

        var cloudiness: String? = null
    }

}