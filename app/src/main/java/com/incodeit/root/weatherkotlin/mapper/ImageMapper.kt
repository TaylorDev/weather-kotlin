package com.incodeit.root.weatherkotlin.mapper

import com.incodeit.root.weatherkotlin.R

object ImageMapper{

    private var imageID: Int = 0

    fun setImage(clouds: String): Int {
        when (clouds) {
            "01d" -> imageID = R.drawable.sun
            "01n" -> imageID = R.drawable.night
            "02d" -> imageID = R.drawable.cloudysun
            "02n" -> imageID = R.drawable.cloudnight
            "03d" -> imageID = R.drawable.cloudy
            "03n" -> imageID = R.drawable.cloudy
            "04d" -> imageID = R.drawable.cloudy
            "04n" -> imageID = R.drawable.cloudy
            "09d" -> imageID = R.drawable.rain
            "09n" -> imageID = R.drawable.rain
            "10d" -> imageID = R.drawable.rain
            "10n" -> imageID = R.drawable.rain
            "11d" -> imageID = R.drawable.thunder
            "11n" -> imageID = R.drawable.thunder
            "13d" -> imageID = R.drawable.snow
            "13n" -> imageID = R.drawable.snow
            "50d" -> imageID = R.drawable.fog
            "50n" -> imageID = R.drawable.fog
        }
        return imageID
    }
}