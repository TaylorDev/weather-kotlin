package com.incodeit.root.weatherkotlin.ui.splash

import android.app.Activity
import com.incodeit.root.weatherkotlin.di.ActivityScope
import com.incodeit.root.weatherkotlin.ui.base.IBasePresenter

@ActivityScope
interface ISplashPresenter<in V : ISplashView> : IBasePresenter<V> {

    fun getUserPreference(activity: Activity): ArrayList<String>

    fun getWeatherNow(city: String)

    fun getWeatherForecast(city: String)

    fun getWeatherForecastDaily(city: String)

}