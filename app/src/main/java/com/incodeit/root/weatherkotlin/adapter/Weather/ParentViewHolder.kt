package com.incodeit.root.weatherkotlin.adapter.Weather

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.CardView
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.incodeit.root.weatherkotlin.R
import com.incodeit.root.weatherkotlin.data.ConverterParameters
import com.incodeit.root.weatherkotlin.mapper.ImageMapper
import com.incodeit.root.weatherkotlin.model.daily_model.Daily
import com.incodeit.root.weatherkotlin.model.daily_model.WeatherDailyParameter
import com.incodeit.root.weatherkotlin.ui.hour_weather_fragment.HourWeatherFragment
import com.incodeit.root.weatherkotlin.ui.weather.WeatherActivity
import com.incodeit.root.weatherkotlin.utils.AppConstants
import com.incodeit.root.weatherkotlin.utils.TimeUtils
import zlc.season.practicalrecyclerview.AbstractAdapter
import zlc.season.practicalrecyclerview.AbstractViewHolder
import java.text.ParseException
import java.text.SimpleDateFormat
import javax.annotation.Nullable

@Suppress("DEPRECATION")
class ParentViewHolder internal constructor(adapter: AbstractAdapter<* , *> , parent: ViewGroup) : AbstractViewHolder<Daily>(parent , R.layout.recyclerview_item_row) {

    @BindView(R.id.item_description)
    @Nullable @JvmField var itemDescriptionTextView: TextView? = null

    @BindView(R.id.item_image)
    @Nullable @JvmField var itemImageView: ImageView? = null

    @BindView(R.id.item_temp)
    @Nullable @JvmField var itemTempTextView: TextView? = null

    @BindView(R.id.weather_card)
    @Nullable @JvmField  var weatherCard: CardView? = null

    private var parent: Daily? = null
    private var child: List<WeatherDailyParameter>? = null
    private val context: Context
    private val adapter: DailyWeatherRecyclerAdapter
    private var imageMapper: ImageMapper? = null

    init {
        ButterKnife.bind(this , itemView)
        context = parent.context
        this.adapter = adapter as DailyWeatherRecyclerAdapter
    }

    override fun setData(data: Daily) {
        if (position == 0) {
            val layoutParams = weatherCard!!.layoutParams as ViewGroup.MarginLayoutParams
            layoutParams.setMargins(30 , 10 , 30 , 10)
            weatherCard!!.cardElevation = 20f
            itemTempTextView!!.setTypeface(null , Typeface.BOLD)
            itemDescriptionTextView!!.setTypeface(null , Typeface.BOLD)
            createHolder(data)
            position.inc()
        } else {
            createHolder(data)
        }
    }

    @SuppressLint("SimpleDateFormat" , "SetTextI18n")
    private fun createHolder(data: Daily) {
        imageMapper = ImageMapper
        if (TimeUtils.getTime() in 5 downTo 20)
            weatherCard!!.setCardBackgroundColor(Color.parseColor("#274356"))
        val originalFormat = SimpleDateFormat("EEE, MMM d, ''yy")
        val targetFormat = SimpleDateFormat("EEE, MMM d")

        try {
            itemDescriptionTextView!!.text = targetFormat.format(originalFormat.parse(data.dateDaily)).toString()
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        itemImageView!!.setImageResource(imageMapper!!.setImage(data.iconDaily!!))
        if (WeatherActivity.pref[0].equals(AppConstants.CELSIUS)) {
            itemTempTextView!!.text = data.temperatureDaily + "\u00B0" + AppConstants.CELSIUS_DATA
        } else {
            itemTempTextView!!.text = ConverterParameters.getFar(data.temperatureDaily!!).toString() + "\u00B0" + AppConstants.FAHRENHEIT_DATA
        }
        child = data.dailyParameters
        parent = data
    }

    @OnClick(R.id.linearWeather)
    fun onClickLinear() {
        transaction()
    }


    @OnClick(R.id.item_description)
    fun onClick() {
        transaction()
    }

    private fun transaction() {
        (context as FragmentActivity).supportFragmentManager
                .beginTransaction()
                .addToBackStack(HourWeatherFragment.TAG)
                .replace(R.id.weather_frame , HourWeatherFragment.newInstance(adapterPosition) , HourWeatherFragment.TAG)
                .commit()
    }

    companion object {
        var position: Int = 0
    }
}