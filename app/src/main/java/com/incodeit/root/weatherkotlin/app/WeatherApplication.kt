package com.incodeit.root.weatherkotlin.app

import android.app.Application
import com.androidnetworking.AndroidNetworking
import com.incodeit.root.weatherkotlin.di.component.ApplicationComponent
import com.incodeit.root.weatherkotlin.di.component.DaggerApplicationComponent
import com.incodeit.root.weatherkotlin.di.module.AppModule
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import javax.inject.Inject

class WeatherApplication : Application() {

    @Inject
    lateinit var calligraphyConfig: CalligraphyConfig

    private lateinit var appComponent : ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerApplicationComponent.builder()
                .appModule(AppModule(this)).build()

        appComponent.inject(this)

        AndroidNetworking.initialize(applicationContext)
        CalligraphyConfig.initDefault(calligraphyConfig)
    }

    fun getComponent() : ApplicationComponent {
        return appComponent
    }

}