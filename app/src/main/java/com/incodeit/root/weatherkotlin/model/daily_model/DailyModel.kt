package com.incodeit.root.weatherkotlin.model.daily_model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DailyModel {

    @SerializedName("city")
    @Expose
    var city: City? = null

    @SerializedName("list")
    @Expose
    var list: kotlin.collections.List<List>? = null

}