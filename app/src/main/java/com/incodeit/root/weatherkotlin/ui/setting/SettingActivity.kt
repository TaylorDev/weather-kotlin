package com.incodeit.root.weatherkotlin.ui.setting

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.WindowManager
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.incodeit.root.weatherkotlin.R
import com.incodeit.root.weatherkotlin.ui.base.BaseActivity
import com.incodeit.root.weatherkotlin.ui.splash.SplashActivity
import com.incodeit.root.weatherkotlin.ui.weather.WeatherActivity
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import java.util.*
import javax.inject.Inject
import com.incodeit.root.weatherkotlin.R.id.cels
import com.incodeit.root.weatherkotlin.utils.TimeUtils
import timber.log.Timber
import javax.annotation.Nullable
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.incodeit.root.weatherkotlin.utils.AppConstants

class SettingActivity : BaseActivity(), ISettingView {

    @BindView(R.id.day_weather)
    @Nullable @JvmField var dayTextView: TextView? = null

    @BindView(R.id.three_days)
    @Nullable @JvmField var weekTextView: TextView? = null

    @BindView(R.id.week_weather)
    @Nullable @JvmField var twoWeekTextView: TextView? = null

    @BindView(R.id.km_weater)
    @Nullable @JvmField var kmTextView: TextView? = null

    @BindView(R.id.mi_weater)
    @Nullable @JvmField var miTextView: TextView? = null

    @BindView(R.id.back_button)
    @Nullable @JvmField var backImageButton: ImageButton? = null

    @BindView(cels)
    @Nullable @JvmField var cellsTextView: ImageView? = null

    @BindView(R.id.far)
    @Nullable @JvmField var farTextView: ImageView? = null

    @BindView(R.id.city_user)
    @Nullable @JvmField var cityUser: TextView? = null

    @BindView(R.id.setting_back)
    @Nullable @JvmField var backgroundSetting: ImageView? = null

    private lateinit var typeface: Typeface

    private var newCity = false

    @Inject
    lateinit var presenter: SettingPresenter<ISettingView>

    private var REQUEST_CODE = 1

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        getActivityComponents().inject(this)
        setUnBinder(ButterKnife.bind(this))
        presenter.onAttach(this@SettingActivity)
        initComponent()
        userPref()
    }

    override fun onBackPressed() {
        backButton()
    }

    override fun userPref() {
        when (WeatherActivity.pref[1]) {
            AppConstants.DAY -> setDay(window.decorView.rootView)
            AppConstants.THREE_DAY -> setThreeDays(window.decorView.rootView)
            AppConstants.WEEK -> setWeek(window.decorView.rootView)
        }
        when (WeatherActivity.pref[0]) {
            AppConstants.CELSIUS -> setCels(window.decorView.rootView)
            AppConstants.FAHRENHEIT -> setFar(window.decorView.rootView)
        }
        val speed = WeatherActivity.pref[2]
        when (speed) {
            AppConstants.SPEED -> setKm(window.decorView.rootView)
            AppConstants.SPEED_US -> setMi(window.decorView.rootView)
        }
    }

    companion object {

        fun getStartIntent(context: Context): Intent {
            return Intent(context , SettingActivity::class.java)
        }

    }

    @OnClick(R.id.search_btn)
    fun setSearch(v: View) {
        initSearch()
    }

    @OnClick(R.id.city_user)
    fun setSearchTextView(v: View) {
        initSearch()
    }

    override fun initSearch() {
        try {
            val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .build(this)
            startActivityForResult(intent , REQUEST_CODE)
        } catch (e: GooglePlayServicesRepairableException) {
            Timber.d("GooglePlayServicesRepairableException %s", e.message)
        } catch (e: GooglePlayServicesNotAvailableException) {
            Timber.d("GooglePlayServicesNotAvailableException %s", e.message)
        }
    }

    override fun onActivityResult(requestCode: Int , resultCode: Int , data: Intent) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val testS = Locale.getDefault().toString()
                if (testS.contains("ru")) {
                    val place = PlaceAutocomplete.getPlace(this , data)
                    val test = place.address.toString().trim { it <= ' ' }.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    cityUser!!.text = place.name.toString()
                    presenter.setUserCity(this , place.name.toString())
                    presenter.setUserRegion(this , test[1])
                    newCity = true
                } else {
                    val place = PlaceAutocomplete.getPlace(this , data)
                    cityUser!!.text = place.name.toString()
                    presenter.setUserCity(this , place.name.toString())
                    presenter.setUserRegion(this , place.name.toString())
                    newCity = true
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                val status = PlaceAutocomplete.getStatus(this , data)
                Timber.i("" , status.statusMessage)
            }
        }
    }

    @OnClick(R.id.back_button)
    fun onBackButton(v: View) {
        backButton()
    }

    private fun backButton() {
        if (!newCity) {
            val intent = WeatherActivity.getStartIntent(this@SettingActivity)
            startActivity(intent)
            finish()
        } else {
            val intent = SplashActivity.getStartIntent(this@SettingActivity)
            startActivity(intent)
            finish()
        }
    }

    override fun initComponent() {
        val fontPath = "fonts/CenturyGothicRegular.ttf"
        typeface = Typeface.createFromAsset(assets , fontPath)
        cityUser!!.text = SplashActivity.pref!![4]
        weekTextView!!.typeface = typeface
        dayTextView!!.typeface = typeface
        twoWeekTextView!!.typeface = typeface
        kmTextView!!.typeface = typeface
        miTextView!!.typeface = typeface
        if (TimeUtils.getTime() in 7..18) {
            backgroundSetting!!.setImageResource(R.drawable.header_day)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(baseContext , R.color.dayPrimary)
            }
        } else {
            backgroundSetting!!.setImageResource(R.drawable.header_night)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(baseContext , R.color.colorPrimary)
            }
        }
    }

    @OnClick(R.id.day_weather)
    fun setDay(v: View) {
        dayTextView!!.setTextColor(ContextCompat.getColor(v.context , R.color.dayPrimary))
        weekTextView!!.setTextColor(ContextCompat.getColor(v.context , R.color.black))
        twoWeekTextView!!.setTextColor(ContextCompat.getColor(v.context , R.color.black))
        presenter.setUserFrequency(this , AppConstants.DAY)
    }

    @OnClick(R.id.three_days)
    fun setThreeDays(v: View) {
        dayTextView!!.setTextColor(ContextCompat.getColor(v.context , R.color.black))
        weekTextView!!.setTextColor(ContextCompat.getColor(v.context , R.color.dayPrimary))
        twoWeekTextView!!.setTextColor(ContextCompat.getColor(v.context , R.color.black))
        presenter.setUserFrequency(this , AppConstants.THREE_DAY)
    }

    @OnClick(R.id.week_weather)
    fun setWeek(v: View) {
        dayTextView!!.setTextColor(ContextCompat.getColor(v.context , R.color.black))
        weekTextView!!.setTextColor(ContextCompat.getColor(v.context , R.color.black))
        twoWeekTextView!!.setTextColor(ContextCompat.getColor(v.context , R.color.dayPrimary))
        presenter.setUserFrequency(this , AppConstants.WEEK)
    }

    @OnClick(R.id.km_weater)
    fun setKm(v: View) {
        miTextView!!.setTextColor(ContextCompat.getColor(v.context , R.color.black))
        kmTextView!!.setTextColor(ContextCompat.getColor(v.context , R.color.dayPrimary))
        presenter.setUserSpeed(this , AppConstants.SPEED)
    }

    @OnClick(R.id.mi_weater)
    fun setMi(v: View) {
        kmTextView!!.setTextColor(ContextCompat.getColor(v.context , R.color.black))
        miTextView!!.setTextColor(ContextCompat.getColor(v.context , R.color.dayPrimary))
        presenter.setUserSpeed(this , AppConstants.SPEED_US)
    }

    @OnClick(cels)
    fun setCels(v: View) {
        cellsTextView!!.setImageResource(R.drawable.celsius)
        farTextView!!.setImageResource(R.drawable.fahrenheit_unselected)
        presenter.setUserTemperature(this , AppConstants.CELSIUS)
    }

    @OnClick(R.id.far)
    fun setFar(v: View) {
        cellsTextView!!.setImageResource(R.drawable.celsius_unselected)
        farTextView!!.setImageResource(R.drawable.fahrenheit)
        presenter.setUserTemperature(this , AppConstants.FAHRENHEIT)
    }

}