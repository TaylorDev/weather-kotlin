package com.incodeit.root.weatherkotlin.adapter.HourWeather

import android.annotation.SuppressLint
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.incodeit.root.weatherkotlin.R
import com.incodeit.root.weatherkotlin.data.ConverterParameters
import com.incodeit.root.weatherkotlin.mapper.ImageMapper
import com.incodeit.root.weatherkotlin.model.forecast_model.Forecast
import com.incodeit.root.weatherkotlin.ui.weather.WeatherActivity
import com.incodeit.root.weatherkotlin.utils.AppConstants
import com.incodeit.root.weatherkotlin.utils.TimeUtils
import java.util.*

class HourWeatherRecyclerAdapter(private val forecasts: ArrayList<Forecast>) : RecyclerView.Adapter<HourWeatherHolder>() {
    private var forecastItem = Forecast()

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): HourWeatherHolder {
        val inflatedView = LayoutInflater.from(parent.context)
                .inflate(R.layout.recyclerview_item_row , parent , false)
        return HourWeatherHolder(inflatedView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: HourWeatherHolder , position: Int) {
        forecastItem = forecasts[position]
        if (WeatherActivity.pref[0].equals(AppConstants.CELSIUS)) {
            holder.weatherTemp.text = forecastItem.temperatureForecast + "\u00B0" + AppConstants.CELSIUS_DATA
        } else {
            holder.weatherTemp.text = ConverterParameters.getFar(forecastItem.temperatureForecast!!).toString() + "\u00B0" + AppConstants.FAHRENHEIT_DATA
        }
        holder.weatherImage.setImageResource(ImageMapper.setImage(forecastItem.iconForecast!!))
        holder.weatherDescription.text = forecastItem.dateForecast
        if (TimeUtils.getTime() in 5 downTo 20)
            holder.weatherCard.setCardBackgroundColor(Color.parseColor("#274356"))
    }

    override fun getItemCount(): Int {
        return forecasts.size
    }
}