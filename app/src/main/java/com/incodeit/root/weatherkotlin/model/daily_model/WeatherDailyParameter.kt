package com.incodeit.root.weatherkotlin.model.daily_model

import com.incodeit.root.weatherkotlin.adapter.Weather.RecyclerItemType

import zlc.season.practicalrecyclerview.ItemType

class WeatherDailyParameter : ItemType {

    var pressureDaily: String? = null

    var humidityDaily: String? = null

    var windDaily: String? = null

    var cloudinessDaily: String? = null

    override fun itemType(): Int {
        return RecyclerItemType.CHILD.value
    }
}