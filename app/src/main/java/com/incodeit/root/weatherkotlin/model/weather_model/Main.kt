package com.incodeit.root.weatherkotlin.model.weather_model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Main {

    @SerializedName("temp")
    @Expose
    private val temp : Double? = null

    @SerializedName("pressure")
    @Expose
    private val pressure : Double? = null

    @SerializedName("humidity")
    @Expose
    private val humidity : Double? = null

    fun getTemp() : Double?{
        return this.temp
    }

    fun getPressure() : Double? {
        return this.pressure
    }

    fun getHumidity() : Double? {
        return this.humidity
    }

}