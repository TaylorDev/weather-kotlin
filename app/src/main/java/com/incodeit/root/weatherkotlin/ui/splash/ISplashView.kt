package com.incodeit.root.weatherkotlin.ui.splash

import com.incodeit.root.weatherkotlin.ui.base.IBaseView
import com.incodeit.root.weatherkotlin.model.weather_model.WeatherUi
import com.incodeit.root.weatherkotlin.model.daily_model.DailyModelUi
import com.incodeit.root.weatherkotlin.model.forecast_model.ForecastUi

interface ISplashView : IBaseView {

    fun successForecast(forecastUi: ForecastUi.Companion)

    fun successDaily(dailyModelUi: DailyModelUi.Companion)

}